import React from 'react';
import ReactDOM from 'react-dom';

function DataForm({onSubmitForm}) {
  function handleSubmit(event) {
    event.preventDefault()
    onSubmitForm(event.target.elements.nameInput.value, event.target.elements.emailInput.value)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="nameInput">Name:</label>
        <input id="nameInput" type="text" />
      </div>
      <div>
        <label htmlFor="emailInput">Email:</label>
        <input id="emailInput" type="text" />
      </div>
      <button type="submit">Send</button>
    </form>
  )
}

function DataDisplay({name, email}) {
  if (name && email) {
    return (
    <div>{name}, {email}</div>
    )
  }
  return (
    <div></div>
  )

}

function App() {
  const [name, setName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const onSubmitForm = (name, email) => {
    setName(name)
    setEmail(email)
    // alert(`You entered: ${username}`
  };
  return (
    <>
    <DataForm onSubmitForm={onSubmitForm} />
    <DataDisplay name={name} email={email}/>
    </>
  )
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
